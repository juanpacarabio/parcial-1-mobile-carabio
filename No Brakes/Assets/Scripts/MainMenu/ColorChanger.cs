using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ColorChanger : MonoBehaviour
{
    public TMPro.TextMeshProUGUI text;

    private Color[] colors = new Color[]
    {
        new Color(1.0f, 0.6f, 0.4f), // Pastel Orange
        new Color(0.6f, 1.0f, 0.6f), // Light Pastel Green
        new Color(1.0f, 0.7f, 0.8f), // Pastel Pink
        new Color(1.0f, 1.0f, 0.4f), // Yellow
        new Color(1.0f, 0.4f, 0.4f)  // Red
    };

    public float transitionDuration = 2f;

    void Start()
    {
        StartCoroutine(ChangeColor());
    }

    IEnumerator ChangeColor()
    {
        int currentColorIndex = 0;

        while (true)
        {
            int nextColorIndex = (currentColorIndex + 1) % colors.Length;
            Color currentColor = colors[currentColorIndex];
            Color nextColor = colors[nextColorIndex];

            float elapsedTime = 0f;

            while (elapsedTime < transitionDuration)
            {
                text.color = Color.Lerp(currentColor, nextColor, elapsedTime / transitionDuration);
                elapsedTime += Time.deltaTime;
                yield return null;
            }
            text.color = nextColor;
            currentColorIndex = nextColorIndex;
        }
    }
}
