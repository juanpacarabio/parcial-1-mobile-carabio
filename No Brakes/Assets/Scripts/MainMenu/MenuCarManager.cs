using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuCarManager : MonoBehaviour
{
    public List<GameObject> Cars = new List<GameObject>();

    public float minInterval = 0.1f;
    public float maxInterval = 2f;

    void Start()
    {
        StartCoroutine(CarActivator());
    }

    IEnumerator CarActivator()
    {
        while (true)
        {
            float waitTime = Random.Range(minInterval, maxInterval);
            yield return new WaitForSeconds(waitTime);

            ActivateCar();
        }
    }

    void ActivateCar()
    {
        List<GameObject> inactiveCars = new List<GameObject>();
        foreach (GameObject car in Cars)
        {
            if (!car.activeInHierarchy)
            {
                inactiveCars.Add(car);
            }
        }

        if (inactiveCars.Count > 0)
        {
            int randomIndex = Random.Range(0, inactiveCars.Count);

            GameObject selectedCar = inactiveCars[randomIndex];
            selectedCar.SetActive(true);

            MenuCars menuCarsScript = selectedCar.GetComponent<MenuCars>();
            if (menuCarsScript != null)
            {
                menuCarsScript.StartingPosition();

                menuCarsScript.speed = Random.Range(5f, 15f);
            }
        }else if (inactiveCars.Count <= 0)
        {
            StopCoroutine(CarActivator());
        }
    }
}
