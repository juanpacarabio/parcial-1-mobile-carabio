using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuCars : MonoBehaviour
{
    public float speed = 10f;
    private void Start()
    {
        StartingPosition();
    }
    void Update()
    {
        transform.Translate(Vector3.forward * speed * Time.deltaTime);
        if (transform.position.z > 100f)
        {
            StartingPosition();
        }
    }

    public void StartingPosition()
    {
        float randomX = Random.Range(-7f, 7f);
        transform.position = new Vector3(randomX, 0f, -20f);
    }
}
