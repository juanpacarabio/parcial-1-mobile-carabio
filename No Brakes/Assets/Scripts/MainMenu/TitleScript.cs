using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleScript : MonoBehaviour
{
    public float amplitudeX = 0.5f;
    public float amplitudeY = 0.5f;

    public float speedX = 1.0f;
    public float speedY = 1.0f;

    private Vector3 initialPosition;

    void Start()
    {
        initialPosition = transform.localPosition;
    }

    void Update()
    {
        float offsetX = Mathf.Sin(Time.time * speedX) * amplitudeX;
        float offsetY = Mathf.Sin(Time.time * speedY) * amplitudeY;

        transform.localPosition = initialPosition + new Vector3(offsetX, offsetY, 0);
    }
}
