using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrivingScript : MonoBehaviour
{
    public GameObject SteeringWheel;
    public GameObject Player;
    public float maxTiltAngle = 45f; 
    public float maxSpeed = 5f; 
    public float smoothing = 0.1f;

    private Vector3 smoothedTilt = Vector3.zero; 
    private float currentTiltAngle = 0f; 

    public static DrivingScript Instance;

    public bool Shielded;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Update()
    {
        if (GameManager.Instance.GameIsRunning == true)
        {
            Vector3 tilt = Input.acceleration;

            smoothedTilt = Vector3.Lerp(smoothedTilt, tilt, smoothing);

            float targetTiltAngle = Mathf.Clamp(smoothedTilt.x * maxTiltAngle, -maxTiltAngle, maxTiltAngle);

            float tiltAngleDelta = targetTiltAngle - currentTiltAngle;
            SteeringWheel.transform.Rotate(0, 0, -tiltAngleDelta);

            currentTiltAngle = targetTiltAngle;

            float speed = Mathf.Clamp(smoothedTilt.x * maxSpeed, -maxSpeed, maxSpeed);
            Player.transform.Translate(speed * Time.deltaTime, 0, 0);
        }
    }

    public void Crash()
    {
        GameManager.Instance.Crash();  
    }
}
