using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : PowerUp
{
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            DrivingScript.Instance.Shielded = true;
            UI_Manager.Instance.EnablePowerUp("Shield");
            this.gameObject.SetActive(false);
        }
        if (collision.gameObject.CompareTag("Obstacle"))
        {
            int direction = Random.Range(0, 2) * 2 - 1;
            transform.position += new Vector3(5 * direction, 0, 0);
        }
    }
}
