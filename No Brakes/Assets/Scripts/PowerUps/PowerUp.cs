using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    float SpawnDistance;
    float speed;
    void Start()
    {
        ResetPosition();
    }

    void Update()
    {
        if (GameManager.Instance.GameIsRunning == true)
        {
            speed = GameManager.Instance.CarSpeed;
            if (transform.position.z < DrivingScript.Instance.gameObject.transform.position.z - 1f)
            {
                this.gameObject.SetActive(false);
            }
            else if (transform.position.z >= DrivingScript.Instance.gameObject.transform.position.z - 1f)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z - speed * Time.deltaTime);
            }
        }
    }

    public void ResetPosition()
    {
        SpawnDistance = Random.Range(20, 60);
        transform.position = new Vector3(DrivingScript.Instance.gameObject.transform.position.x + Random.Range(-10, 10), transform.position.y, SpawnDistance);
        transform.rotation = Quaternion.Euler(0, 0, 0);
    }
}
