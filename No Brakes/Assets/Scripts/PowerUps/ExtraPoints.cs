using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtraPoints : PowerUp
{
    public float AdditionalPoints;
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            GameManager.Instance.Score = GameManager.Instance.Score + AdditionalPoints;
            this.gameObject.SetActive(false);
        }
    }
}
