using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Manager : MonoBehaviour
{
    [SerializeField] TMPro.TextMeshProUGUI Score, Score2;
    [SerializeField] GameObject ShieldIcon;
    private int CurrentScore;
    public static UI_Manager Instance;  
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {
        
    }

    void Update()
    {
        CurrentScore = (int)GameManager.Instance.Score;
        Score.text = CurrentScore.ToString() + " pts";
        Score2.text = Score.text;
    }

    public void EnablePowerUp(string power)
    {
        if (power == "Shield")
        {
            ShieldIcon.SetActive(true);
        }
    }

    public void DisablePowerUp(string power)
    {
        if (power == "Shield")
        {
            ShieldIcon.SetActive(false);
        }
    }
}
