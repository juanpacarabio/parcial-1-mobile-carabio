using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public float CarSpeed = 3f;
    public float Acceleration = 0.1f;
    public float Score = 0;
    float ScoreMultiplier = 1;
    float lastSpeedCheck = 3f;
    public bool GameIsRunning = true;
    [SerializeField] GameObject GameOverScreen, UI_Screen;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        StartCoroutine(IncrementScore());
    }

    private void Update()
    {
        if (GameIsRunning == true)
        {
            CarSpeed += Acceleration * Time.deltaTime;

            if (CarSpeed >= lastSpeedCheck + 3f)
            {
                ScoreMultiplier += 1f;
                lastSpeedCheck = CarSpeed;
            }
        }
    }

    public void MinorCrash()
    {
        ScreenShake.Instance.ShakeScreen();
        ScoreMultiplier = 1;
        CarSpeed = CarSpeed / 2;
        lastSpeedCheck = CarSpeed;
    }

    public void Crash()
    {
        GameIsRunning = false;
        UI_Screen.SetActive(false);
        GameOverScreen.SetActive(true);
        GameOverManager.Instance.SetUpGameOver();
    }

    IEnumerator IncrementScore()
    {
        while (GameIsRunning == true)
        {
            Score += 1 * ScoreMultiplier;
            yield return new WaitForSeconds(1f);
        }
    }
}
