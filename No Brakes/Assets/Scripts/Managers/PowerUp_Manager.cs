using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp_Manager : MonoBehaviour
{
    public List<GameObject> PowerUps = new List<GameObject>();
    public float minInterval = 8f;
    public float maxInterval = 13f;

    void Start()
    {
        StartCoroutine(PowerUpActivator());
    }

    IEnumerator PowerUpActivator()
    {
        while (GameManager.Instance.GameIsRunning == true)
        {
            float waitTime = Random.Range(minInterval, maxInterval);
            yield return new WaitForSeconds(waitTime);
            ActivatePowerUp();
        }
    }

    void ActivatePowerUp()
    {
        List<GameObject> inactivePowerUps = new List<GameObject>();
        foreach (GameObject powerUp in PowerUps)
        {
            if (!powerUp.activeInHierarchy)
            {
                inactivePowerUps.Add(powerUp);
            }
        }

        if (inactivePowerUps.Count > 0)
        {
            int randomIndex = Random.Range(0, inactivePowerUps.Count);

            GameObject selectedPowerUp = inactivePowerUps[randomIndex];
            selectedPowerUp.SetActive(true);

            PowerUp powerUpScript = selectedPowerUp.GetComponent<PowerUp>();
            if (powerUpScript != null)
            {
                powerUpScript.ResetPosition();
            }
        }
    }
}
