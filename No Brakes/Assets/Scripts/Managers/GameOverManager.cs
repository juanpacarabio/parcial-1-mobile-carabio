using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverManager : MonoBehaviour
{
    [SerializeField] TMPro.TextMeshProUGUI score;
    public static GameOverManager Instance;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    public void SetUpGameOver()
    {
        score.text = GameManager.Instance.Score.ToString() + "pts";
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene("MenuScene");
    }

    public void Replay()
    {
        SceneManager.LoadScene("GameScene");
    }
}
