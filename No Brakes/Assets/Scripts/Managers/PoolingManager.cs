using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolingManager : MonoBehaviour
{
    public List<GameObject> Obstacles = new List<GameObject>();
    public float TimeBetweenSpawns = 5f;

    void Start()
    {
        for (int i = 0; i < 4 && Obstacles.Count > 0; i++)
        {
            ActivateObstacle();
        }
        StartCoroutine(ObstacleActivator());
    }

    void ActivateObstacle()
    {
        if (Obstacles.Count > 0)
        {
            int random = Random.Range(0, Obstacles.Count);

            Obstacles[random].SetActive(true);

            Obstacles.RemoveAt(random);
        }
    }

    IEnumerator ObstacleActivator()
    {
        while (Obstacles.Count > 0 && GameManager.Instance.GameIsRunning == true)
        {
            yield return new WaitForSeconds(TimeBetweenSpawns);
            ActivateObstacle();
        }
    }
}
