using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    public  float speed;
    public float SpawnDistance;
    public GameObject Player;
    private void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
        ResetPosition();
    }

    private void Update()
    {
        if (GameManager.Instance.GameIsRunning == true)
        {
            speed = GameManager.Instance.CarSpeed;
            if (transform.position.z < DrivingScript.Instance.gameObject.transform.position.z - 1f)
            {
                ResetPosition();
            }
            else if (transform.position.z >= DrivingScript.Instance.gameObject.transform.position.z - 1f)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z - speed * Time.deltaTime);
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (DrivingScript.Instance.Shielded == false)
            {
                DrivingScript.Instance.Crash();
            }else if (DrivingScript.Instance.Shielded == true)
            {
                DrivingScript.Instance.Shielded = false;
                UI_Manager.Instance.DisablePowerUp("Shield");
                ScreenShake.Instance.ShakeScreen();
            }
            ResetPosition();
        }
        if (collision.gameObject.CompareTag("Obstacle"))
        {
            int direction = Random.Range(0, 2) * 2 - 1;
            transform.position += new Vector3(5 * direction, 0, 0);
        }
    }

    public void ResetPosition()
    {
        SpawnDistance = Random.Range(20, 60);
        transform.position = new Vector3(DrivingScript.Instance.gameObject.transform.position.x + Random.Range(-10,10), transform.position.y, SpawnDistance);
        transform.rotation = Quaternion.Euler(0, 0, 0);
    }
}
