using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Roller : Bump
{
    // Start is called before the first frame update
    void Start()
    {
        ResetPosition();
    }

    public override void Update()
    {
        base.Update();
        transform.position = new Vector3(transform.position.x - 5 * Time.deltaTime, transform.position.y, transform.position.z);
    }

    public override void ResetPosition()
    {
        SpawnDistance = Random.Range(10, 30);
        transform.position = new Vector3(DrivingScript.Instance.gameObject.transform.position.x + Random.Range(10, 40), transform.position.y, SpawnDistance);
        transform.rotation = Quaternion.Euler(0, 0, 0);
    }
}
