using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bump : MonoBehaviour
{
    public float speed;
    public float SpawnDistance;
    void Start()
    {
        ResetPosition();
    }

    public virtual void Update()
    {
        if (GameManager.Instance.GameIsRunning == true)
        {
            speed = GameManager.Instance.CarSpeed;
            if (transform.position.z < DrivingScript.Instance.gameObject.transform.position.z - 1f)
            {
                ResetPosition();
            }
            else if (transform.position.z >= DrivingScript.Instance.gameObject.transform.position.z - 1f)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z - speed * Time.deltaTime);
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (DrivingScript.Instance.Shielded == false)
            {
                GameManager.Instance.MinorCrash();
            }
            else if (DrivingScript.Instance.Shielded == true)
            {
                DrivingScript.Instance.Shielded = false;
                UI_Manager.Instance.DisablePowerUp("Shield");
            }
            ResetPosition();
        }
        if (collision.gameObject.CompareTag("Obstacle"))
        {
            int direction = Random.Range(0, 2) * 2 - 1;
            transform.position += new Vector3(5 * direction, 0, 0);
        }
    }

    public virtual void ResetPosition()
    {
        SpawnDistance = Random.Range(20, 60);
        transform.position = new Vector3(DrivingScript.Instance.gameObject.transform.position.x + Random.Range(-10, 10), transform.position.y, SpawnDistance);
        transform.rotation = Quaternion.Euler(0, 0, 0);
    }
}
